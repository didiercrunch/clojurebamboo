(ns myplugin.core
    (:import (com.atlassian.bamboo.task TaskResultBuilder)))

(gen-class
    :name "some.myplugin.bambootask"
    :implements [com.atlassian.bamboo.task.TaskType]
    :prefix "plugin-")

(defn plugin-execute [this taskcontex]
    (.build (TaskResultBuilder/newBuilder taskcontex)))
