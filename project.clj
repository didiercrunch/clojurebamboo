(defproject myplugin "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [
    [org.clojure/clojure "1.8.0"]
    [com.atlassian.bamboo/atlassian-bamboo-web "5.14.3"]
    [com.atlassian.plugin/atlassian-spring-scanner-annotation "1.2.13"]
  ]
  :repositories {"atlassian" "https://maven.atlassian.com/content/groups/public/"}
  :target-path "target/%s"
  :resource-paths ["atlassian-plugin.xlm"]
  :profiles {
      :uberjar {
          :aot :all
          :provided {:dependencies [[com.atlassian.bamboo/atlassian-bamboo-web "5.14.3"]
                                    [com.atlassian.plugin/atlassian-spring-scanner-annotation "1.2.13"]]}}
         })
